<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/register', 'UserController@store');
Route::post('/login','UserController@index');

Route::post('/posts', 'BlogController@store');
Route::get('/posts', 'BlogController@index');
Route::get('/posts/{id}', 'BlogController@list');
Route::patch('/posts/{slug}', 'BlogController@update');

Route::post('/posts/{slug}/comments', 'CommentsController@store');
Route::get('/posts/{slug}/comments', 'CommentsController@list');