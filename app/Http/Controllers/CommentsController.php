<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\comments;
use App\User;

class CommentsController extends Controller
{

	public function list(){
    	$getComments = Comments::all();

    	return $getComments->toJson();
    }

    public function store(\Illuminate\Http\Request $request, $slug){
    	$token = explode(' ', $request->header('Authorization'));
    	$getUserId = User::where('remember_token',$token[1])->first();

    	$storeComments = comments::create([
    		'body' => request('body'),
    		'slug' => $slug,
    		'commentable_type' => 'App\\Post',
    		'commentable_id' => '1',
    		'creator_id' => $getUserId->id
    	]);

    	return $storeComments->toJson();
    }



}
