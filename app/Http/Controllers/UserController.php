<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\User;

class UserController extends Controller
{
    public function list(){
    	$getUsers = User::all();

    	return $getusers->toJson();
    }

    public function store(){
    	$addUsers = User::create([
    		'name' => request('name'),
    		'email' => request('email'),
    		'password' => request('password'),
    		'password_confirmation' => request('password_confirmation'),
    		'remember_token' => (string) Str::random(80)
    	]);

    	return $addUsers->toJson();
    }

    public function index(){
    	date_default_timezone_set('Asia/Manila');
    	$loginUser = User::where('email',request('email'))->where('password',request('password'))->first();

    	if($loginUser){
    		return response()->json([
    			'token' => $loginUser->remember_token,
    			'token_type' => 'bearer'
    		]);
    	}
    }
}
