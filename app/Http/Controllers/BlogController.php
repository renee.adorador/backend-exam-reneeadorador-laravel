<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\blogPosts;
use App\User;

class BlogController extends Controller
{
    //list
    public function list($id){
    	$getPostsData = blogPosts::where('title',$id)->first();

    	return response()->json([
    		'data' => $getPostsData
    	]);
    }

    public function index(){
    	$getPosts = blogPosts::paginate(15);

    	return $getPosts->toJson();
    }
	//store
    public function store(\Illuminate\Http\Request $request){
    	$token = explode(' ', $request->header('Authorization'));
    	$getUserId = User::where('remember_token',$token[1])->first();


    	$storePosts = blogPosts::create([
    		'title' => request('title'),
    		'content' => request('content'),
    		'image' => request('image'),
    		'slug' => request('title'),
    		'user_id' => $getUserId->id
    	]);

    	return $storePosts->toJson();
    }
    //update
    public function update(\Illuminate\Http\Request $request,$slug){
    	$token = explode(' ', $request->header('Authorization'));
    	$getUserId = User::where('remember_token',$token[1])->first();

    	$patchPosts = blogPosts::where('user_id',$getUserId->id)->where('slug',$slug)->update([
    		'title' => request('title'),
    		'content' => request('content'),
    		'image' => request('image'),
    		'slug' => request('title')
    	]);

    	if($patchPosts){
    		$getNewPosts = blogPosts::where('user_id',$getUserId->id)->where('slug',$slug)->first();

    		return response()->json([
	    		'data' => $getNewPosts
	    	]);
    	}
    }
}
